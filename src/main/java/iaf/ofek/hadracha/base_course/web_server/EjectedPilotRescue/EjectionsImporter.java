package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.CrudDataBase;
import iaf.ofek.hadracha.base_course.web_server.Data.Entity;
import iaf.ofek.hadracha.base_course.web_server.Utilities.ListOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class EjectionsImporter {

  @Value("${ejections.server.url}")
  public String EJECTION_SERVER_URL;

  @Value("${ejections.namespace}")
  public String NAMESPACE;

  private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
  private final RestTemplate restTemplate;
  private final CrudDataBase dataBase;
  private final ListOperations listOperations;
  private static final Double SHIFT_NORTH = 1.7;

  public EjectionsImporter(
      RestTemplateBuilder restTemplateBuilder,
      CrudDataBase dataBase,
      ListOperations listOperations) {
    restTemplate = restTemplateBuilder.build();
    this.dataBase = dataBase;
    this.listOperations = listOperations;
    executor.scheduleAtFixedRate(this::updateEjections, 1, 1, TimeUnit.SECONDS);
  }

  private void updateEjections() {
    try {
      List<EjectedPilotInfo> ejectionsFromServer;
      ResponseEntity<List<EjectedPilotInfo>> responseEntity =
          restTemplate.exchange(
              EJECTION_SERVER_URL + "/ejections?name=" + NAMESPACE,
              HttpMethod.GET,
              null,
              new ParameterizedTypeReference<List<EjectedPilotInfo>>() {});
      ejectionsFromServer = responseEntity.getBody();
      if (ejectionsFromServer != null) {
        fixPilotsLocation(ejectionsFromServer);
      }
      List<EjectedPilotInfo> updatedEjections = ejectionsFromServer;
      List<EjectedPilotInfo> previousEjections = dataBase.getAllOfType(EjectedPilotInfo.class);

      addEjections(updatedEjections, previousEjections);
      removeEjections(updatedEjections, previousEjections);

    } catch (RestClientException e) {
      System.err.println("Could not get ejections: " + e.getMessage());
      e.printStackTrace();
    }
  }
    //בהמשך לשיחה שלנו עם אביב ,לגבי האם לשנות את כפל הקוד
   // ו ejectionsToAdd  ejectionsToRemove שמנו לב שהכפל קוד הוא בפעולות
  // החלטנו לא לשנות את הפעולות ולאחד אותן לאחת כי לדענו זה ישבור את חוקי הclean code ולא יהיה מובן מה הפעולה תעשה
  // ובכל זאת איך שהיינו ממשים את הפעולה החדשה היא בדיוק אותו דבר רק שפעם אחת קוראים לפרמטרים updateEjections  לפני previousEjection
  // ופעם אחרת קוראים להם בסדר הפוך
  private List<EjectedPilotInfo> ejectionsToRemove(
      List<EjectedPilotInfo> updatedEjections, List<EjectedPilotInfo> previousEjections) {
    return listOperations.subtract(
        previousEjections, updatedEjections, new Entity.ByIdEqualizer<>());
  }

  private List<EjectedPilotInfo> ejectionsToAdd(
      List<EjectedPilotInfo> updatedEjections, List<EjectedPilotInfo> previousEjections) {
    return listOperations.subtract(
        updatedEjections, previousEjections, new Entity.ByIdEqualizer<>());
  }

  private void fixPilotsLocation(List<EjectedPilotInfo> pilots) {
    for (EjectedPilotInfo ejectedPilotInfo : pilots) {
      ejectedPilotInfo.getCoordinates().lat += SHIFT_NORTH;
    }
  }

  private void addEjections(
      List<EjectedPilotInfo> updatedEjections, List<EjectedPilotInfo> previousEjections) {
    List<EjectedPilotInfo> addedEjections = ejectionsToAdd(updatedEjections, previousEjections);
    addedEjections.forEach(dataBase::create);
  }

  private void removeEjections(
      List<EjectedPilotInfo> updatedEjections, List<EjectedPilotInfo> previousEjections) {
    List<EjectedPilotInfo> removedEjections =
        ejectionsToRemove(updatedEjections, previousEjections);
    removedEjections.stream()
        .map(EjectedPilotInfo::getId)
        .forEach(id -> dataBase.delete(id, EjectedPilotInfo.class));
  }


}
